---
title: "June, 2021"
date: 2021-06-01T10:51:07+03:00
author: "Andrew Espira"
categories: ["Notes"]
---

## 2021-09-01
- topics covered in DevOps workshop
   - How to stand-up GitLab Ultimate in AWS via the marketplace.
   - How to install GitLab Runner and register it to our GitLab project.
   - How to create a new project and import a repository to it.
   - How to configure GitLab CI/CD via the .gitlab-ci.yml file.
   - How to save secrets in GitLab Variables in the CI/CD settings.
   - How to use GitLab DevOps platform to build, test and deploy applications.



<!--more-->



<!-- vim: set sw=2 ts=2: -->
